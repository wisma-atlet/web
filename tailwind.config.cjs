/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{svelte,html,ts,js}"
    ],
    theme: {
        extend: {},
    },
    plugins: [],
}

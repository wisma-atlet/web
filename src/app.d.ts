// See https://kit.svelte.dev/docs/types#app

import type { EventHandler } from "$lib/server/adapter";

// for information about these interfaces
declare global {
    namespace Lucia {
        type Auth = import("$lib/server/adapter").Auth
        type UserAttributes = {
            email: string,
        }

    }
    namespace App {
        // interface Error {}
        interface Locals {
            auth: EventHandler
        }
        interface PageData {
            flash?: { type: "success" | "error" | "warning", message }[]
        }
        // interface Platform {}
    }
}

export { };

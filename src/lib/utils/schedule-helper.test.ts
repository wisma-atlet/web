import dayjs from "dayjs"
import { expect, describe, it } from "vitest"
import { generateInterval, generateFreeTime } from "./schedule-helper"

describe("Testing schedule-helper", () => {

    const schedules = [
        {
            start_at : new Date('2022-02-02T22:00:00'),
            end_at   : new Date('2022-02-03T00:59:59.999'),
        }
    ]

    const start_t = new Date('2022-02-02T00:00:00')

    it("Testing generate interval", () => {
        const interval = 10 * 60 * 1000
        const generated_tade = [ ...generateInterval(dayjs(start_t), dayjs(schedules[0].start_at).subtract(1, 'millisecond'), interval) ]

        expect(generated_tade[0], "Check the first generated item").eq(start_t.getTime())
        expect(generated_tade.at(-1), 'Check last generated item').eq(schedules[0].start_at.getTime() - interval)
        expect(generated_tade.length, 'Check generated items length').eq((schedules[0].start_at.getTime() - start_t.getTime()) / interval)

    })

    it("Testing generate interval with default interval", () => {
        const interval = 30 * 60 * 1000
        const generated_tade = [ ...generateInterval(dayjs(start_t), dayjs(schedules[0].start_at).subtract(1, 'millisecond'), interval) ]

        expect(generated_tade[0], "Check the first generated item").eq(start_t.getTime())
        expect(generated_tade.at(-1), 'Check last generated item').eq(schedules[0].start_at.getTime() - interval)
        expect(generated_tade.length, 'Check generated items length').eq((schedules[0].start_at.getTime() - start_t.getTime()) / interval)
    })

})

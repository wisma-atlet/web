import dayjs, { Dayjs } from 'dayjs'
type Schedule = {
    start_at: Date,
    end_at: Date
}

const DEFAULT_INTERVAL = 30 * 60 * 1000

export function* generateInterval(
    start_at: Dayjs,
    end_at: Dayjs,
    interval = DEFAULT_INTERVAL
): Generator<number, void, number | null | undefined> {
    const start_at_value = start_at.valueOf();
    const end_at_value = end_at.valueOf() - interval + 1

    if (start_at_value === end_at_value) return
    let next: number | null | undefined = null;

    for (
        let i = start_at_value;
        i <= end_at_value;
    ) {
        next = yield i

        if (next) {
            i = next
            continue
        }

        i += interval
    }

}

function* compactSchedule(schedules: { start_at: number, end_at: number }[]) {
    let start_at: number = schedules[0].start_at;
    let end_at: number = schedules[0].end_at

    for (const i of schedules.slice(1)) {
        if (end_at + 1 !== i.start_at) {
            yield {
                start_at,
                end_at
            }
            start_at = i.start_at;
        }
        end_at = i.end_at

    }
    yield {
        start_at,
        end_at
    }
}

export function* generateFreeTime(schedules: Schedule[], start_at: Date, interval = DEFAULT_INTERVAL) {
    let start_at_day = dayjs(start_at)
    const end_at_day = start_at_day.add(7, 'day').endOf('day')

    if (schedules.length === 0) {
        for (const i of generateInterval(start_at_day, end_at_day)) {
            yield [ i, false ] as const
        }
        return
    }

    const schedules_value = Array.from(compactSchedule(schedules
        .map(e => ({
            start_at : e.start_at.getTime(),
            end_at   : e.end_at.getTime()
        }))))

    let next_schedule = 0
    if (
        schedules_value[0].start_at < start_at.getTime() &&
        start_at.getTime() < schedules_value[0].end_at
    ) {
        start_at_day = dayjs(schedules_value[0].end_at + 1)
        next_schedule = 1;
    }

    const generator = generateInterval(start_at_day, end_at_day)

    let next: IteratorResult<number, void>;
    let skip: number | null = null

    while (!(next = generator.next(skip)).done) {
        skip = null

        if (
            (next_schedule < schedules_value.length) &&
            (schedules_value[next_schedule].start_at - interval === next.value)
        ) {
            skip = schedules_value[next_schedule].end_at + 1
            next_schedule++
        }

        yield [ next.value, skip !== null ] as const
    }

}

export function createSchedule(schedules: Schedule[], start_at: Date) {
    let next: IteratorResult<readonly [number, boolean], void>;

    const generator = generateFreeTime(schedules, start_at)

    const result: [number, number[]][] = []

    const date_end: number[] = [];

    let temp: number[] = []
    let current = dayjs(start_at).startOf('day');

    while (!(next = generator.next()).done) {
        next.value[1] && date_end.push(next.value[0] + DEFAULT_INTERVAL)

        if (!current.isSame(next.value[0], 'day')) {
            result.push([ current.valueOf(), temp ])
            temp = [];

            current = dayjs(next.value[0]).startOf('days')
        }

        temp.push(next.value[0])
    }

    return {
        map  : result,
        ends : date_end
    }
}

import { z } from 'zod'

export const createRentValidator = z.object({
    owner : z.string({
        invalid_type_error : "Nama Penyewa harus berupa text",
        required_error     : "Nama penyewa harus diisi"
    }).min(1, "Nama pemilik setidaknya 1 karakter"),

    email : z.string({
        invalid_type_error : "Email penyewa harus berupa text",
        required_error     : "Email penyewa harus diisi"
    }).email("Format email penyewa salah"),

    start_at : z.coerce.number({
        invalid_type_error : "Format Tanggal Salah",
        required_error     : "Tanggal mulai diperlukan"
    }),

    duration : z.coerce.number({
        invalid_type_error : "Durasi harus berupa angka",
        required_error     : "Durasi harus dipilih"
    }).gte(1, "Durasi setidaknya 1 jam")
        .lte(4, "Durasi maksimal 4 jam"),

    field_id : z.coerce.number({
        invalid_type_error : "Lapangan tidak valid",
        required_error     : "Lapangan harus di pilih"
    }).gt(0, "lapangan harus dipilih"),
})

export default createRentValidator

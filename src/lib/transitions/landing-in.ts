import { linear } from "svelte/easing";

export default function(_, status: boolean) {
    return {
        delay    : 0,
        duration : 500,
        easing   : linear,
        css      : (_t: number, u: number) => {
            if (status) return `transform: translateX(${u * 100}%)`;
            return `transform: translateX(${-u * 100}%)`;
        },
    };
}

import { quadIn } from "svelte/easing";
import type { TransitionConfig } from "svelte/types/runtime/transition";

export default function(_node: HTMLDivElement): TransitionConfig {
    return { delay : 0, duration : 500, easing : quadIn, css : (_t: number, u: number) => `transform: translateX(${-u * 100}%)`, };
}

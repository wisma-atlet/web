import Adapter from "@wisma-atlet/database";

const adapter = new Adapter()

export default adapter.mainSite
export type Auth = typeof adapter.auth
export type EventHandler = ReturnType<typeof adapter.mainSite.handleEvent>

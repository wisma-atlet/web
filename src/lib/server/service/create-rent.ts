import createRentValidator from "$lib/validator/create-rent-validator";
import { z } from "zod";
import adapter from "../adapter";

const createRent = createRentValidator.transform(async (data, context) => {
    try {
        return await adapter.mainSite.createRent(
            data.field_id,
            data.start_at,
            data.duration,
            data.owner,
            data.email
        );
    } catch (e) {
        if (!(e instanceof Error)) throw e

        context.addIssue({
            message : e.message as string,
            code    : 'custom',
            fatal   : true
        })

        return z.NEVER
    }
});

export default createRent;

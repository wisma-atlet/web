// import adapter from '$lib/server/adapter'
import type { Handle } from '@sveltejs/kit'

export const handle = (async ({ event, resolve }) => {
    // event.locals.auth = adapter.handleEvent(event)
    return await resolve(event)
}) satisfies Handle
